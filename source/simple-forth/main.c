#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

struct word {
    char* name;
    size_t length;
    size_t line;
    size_t column;
};

static char* open_file(const char* name) {
    FILE* file = fopen(name, "r");
    if (!file) { perror(name); exit(1); }
    fseek(file, 0, SEEK_END);
    const size_t length = ftell(file);
    char* text = calloc(length + 1, sizeof(char));
    fseek(file, 0, SEEK_SET);
    fread(text, sizeof(char), length, file);
    fclose(file);
    return text;
}


///TODO: simplify me. replace all whitespace with 0. then just get the stringwords amidst the zeros.

static struct word* lex(char* text, size_t* word_count) {
    struct word * words = NULL, current = {0};
    size_t count = 0, line = 1, column = 1;
    bool in_word = false;
    const size_t text_length = strlen(text);
    
    for (size_t i = 0; i < text_length; i++) {
        const char c = text[i];
        if (!isspace(c) && !in_word) { current = (struct word){text + i, 1, line, column}; in_word = true; }
        else if (!isspace(c) && in_word) current.length++;
        else {
            if (current.length) {
                current.name[current.length] = '\0';
                words = realloc(words, sizeof(struct word) * (count + 1));
                words[count++] = current;
                current = (struct word){0};
            } in_word = false;
        }
        if (c == '\n') {
            line++;
            column = 1;
        } else column++;
    }            
    *word_count = count;
    return words;
}

static void debug_tokens(struct word* words, const size_t count) {
    printf("printing tokens:\n");
    for (size_t i = 0; i < count; i++)
        printf("TOKEN( name: %s, length: %lu, line: %lu, col: %lu)\n", words[i].name, words[i].length, words[i].line, words[i].column);
    printf("printed all tokens.\n");
}

static void print_stack(const ssize_t* stack, const size_t count) {
    printf("{ ");
    for (size_t i = 0; i < count; i++) {
        printf("%ld ", stack[i]);
    }
    printf("}\n");
}

static void interpret(struct word* words, const size_t word_count) {
    ssize_t * stack = NULL, count = 0;
    ssize_t * call_stack = NULL, call_count = 0;
    for (size_t i = 0; i < word_count; i++) {
        struct word w = words[i];
                        
        if (false) {}
        else if (!strcmp("define", w.name)) {
             ///TODO: code me.
        }
        ///TODO: allow for lambdas!
                
        else if (!strcmp("emit", w.name) && count) printf("%c", (char) stack[count - 1]);
        else if (!strcmp("print", w.name) && count) printf("%ld\n", stack[count - 1]);
        else if (!strcmp("printn", w.name) && count) printf("%ld ", stack[count - 1]);
        else if (!strcmp("`", w.name)) print_stack(stack, count);
        else if (!strcmp("``", w.name)) print_stack(call_stack, call_count);
        else if (!strcmp(".", w.name) && count) count--;
        else if (!strcmp("..", w.name) && call_count) call_count--;
        else if (!strcmp("++", w.name) && count) stack[count - 1]++;
        else if (!strcmp("--", w.name) && count) stack[count - 1]--;
        else if (!strcmp(":", w.name) && count) stack[count - 1] = stack[count - 1 - stack[count - 1]];
        else if (!strcmp("!", w.name) && count) stack[count - 1] = !stack[count - 1];
        else if (!strcmp("neg", w.name) && count) stack[count - 1] = -stack[count - 1];
        
        else if (!strcmp("]", w.name)) {}
        else if (!strcmp("[", w.name) && count) { if (!stack[count - 1]) while (strcmp("]", words[i].name) && i++ < word_count); }
        ///TODO: make [ ] nest properly, by using the stack as you should, instead of just skipping ahead.
        
        else if (!strcmp("{", w.name)) { call_stack = realloc(call_stack, sizeof(ssize_t) * (call_count + 1)); call_stack[call_count++] = i; }
        else if (!strcmp("}", w.name) && count && call_count) { if (stack[count - 1]) i = call_stack[call_count - 1]; else call_count--; }
        else if (!strcmp("?", w.name) && count >= 3) { stack[count - 3] = stack[count - 1] ? stack[count - 2] : stack[count - 3]; count -= 2; }
        else if (!strcmp("+", w.name) && count >= 2) { stack[count - 2] = stack[count - 2] + stack[count - 1]; count--; }
        else if (!strcmp("*", w.name) && count >= 2) { stack[count - 2] = stack[count - 2] * stack[count - 1]; count--; }
        else if (!strcmp("-", w.name) && count >= 2) { stack[count - 2] = stack[count - 2] - stack[count - 1]; count--; }
        else if (!strcmp("/", w.name) && count >= 2) { stack[count - 2] = stack[count - 2] / stack[count - 1]; count--; }
        else if (!strcmp("%", w.name) && count >= 2) { stack[count - 2] = stack[count - 2] % stack[count - 1];  count--; }
        else if (!strcmp("and", w.name) && count >= 2) { stack[count - 2] = stack[count - 2] && stack[count - 1]; count--; }
        else if (!strcmp("or", w.name) && count >= 2) { stack[count - 2] = stack[count - 2] || stack[count - 1]; count--; }
        else if (!strcmp("=", w.name) && count >= 2) { stack[count - 2] = (stack[count - 2] == stack[count - 1]); count--; }
        else if (!strcmp("<", w.name) && count >= 2) { stack[count - 2] = (stack[count - 2] < stack[count - 1]); count--; }
        else {
            const ssize_t n = atoll(w.name);
            stack = realloc(stack, sizeof(ssize_t) * (count + 1)); stack[count++] = n;
            if (!n && strcmp("0", w.name)) printf("sf: %lu:%lu: error: unidentified word: %s\n", w.line, w.column, w.name);
        }
    } free(stack); free(call_stack);
}

int main(const int argc, const char** argv) {
    if (argc <= 1) { printf("error: no input files.n\n"); exit(1); }
    char* text = open_file(argv[1]);
    size_t word_count = 0;
    struct word* words = lex(text, &word_count);
    interpret(words, word_count);
    free(words);
    free(text);
}
