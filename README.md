# simple-forth


simple forth is the beginnings of an interpreter programming language (written in C) created by Daniel Rehman, and is a simple version of the forth programming language, as the name would imply. 

this means that it is stack based, and uses reverse polish notation for all function calls. furthermore, the language aims to unify many of the words that are actually reflection of the same operation. 

eg, swap and rot in forth are really a special case of the rotate-n operation, where n is 2 or 3 respecitvely. things like that, is what this language aims to reduce. furthermore, the language has debug facilities, and nicer and terser syntax for most operations. the language is not complete, however.

stay tuned for this languages development! lambdas, nested conditionals, more stack operators (like rotate-n) and other cool stuff are coming soon for this language! 

